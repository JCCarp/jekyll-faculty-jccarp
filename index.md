---
layout: default
---

## Quickstart Guide

Click [here](https://bitbucket.org/UBCCS/jekyll-faculty/src/master/) to return to the 
repository website that shows instructions on how to use
and customize this UBC CS faculty and course static website generator yourself.  Browse
the markdown files in particular (```.md``` file extension; start with ```index.md``` -- make sure you view the raw source).

Browse the remainder of this demonstration website to gain a sense of what can
be easily and efficiently generated and maintained (like $x^2 + 1 = 17$, ```while(true) ++i```, and the references below).

---------------
## About Me

<img class="profile-picture" src="headshot.jpg">

Welcome to my home page. As an academic on paper, I go by the alias [Alexander J. Summers](https://scholar.google.ch/citations?user=q1bbqYEAAAAJ&hl=en), but in other capacities you can call me Alex.

In March 2020 I joined the [Department of Computer Science](https://www.cs.ubc.ca/) at the University of British Columbia (UBC), as an Associate Professor. I'm looking for strong postdocs, PhD, MSc and internship students to work with on a broad variety of topics connected to program correctness (see below). Prior to coming to UBC I worked at [ETH Zürich](https://www.ethz.ch/en.html) as a senior researcher (Deutsch: Oberassistent II), in the [Chair of Programming Methodology group](http://www.pm.inf.ethz.ch/) run by Peter Müller.

## Research Interests
I work in the general area of program correctness, including developing new specification and verification logics and type systems, and developing automated tools for constructing proofs about heap-based and concurrent programs, usually building on SMT solvers. For many years, I co-ordinated the [Viper Project](http://www.pm.inf.ethz.ch/research/viper.html), in which we develop a new intermediate verification language and tool suite designed to ease the construction of new verification tools for modern reasoning techniques.

I recently started the [Prusti Project](https://www.pm.inf.ethz.ch/research/prusti.html), in which we are building the first deductive verification tools for the [Rust programming language](https://www.rust-lang.org/). More generally, I mainly work in the area of software verification for concurrent and object-oriented programs, and I'm delighted to have been awarded the [2015 Dahl-Nygaard Junior Prize](http://www.aito.org/Dahl-Nygaard/2015.html) for my work in this area. This was awarded at [ECOOP 2015](http://2015.ecoop.org/). More information about my research can be found [here](http://people.inf.ethz.ch/summersa/wiki/doku.php?id=research).

Some personal (but not too personal) things can be found [here](http://people.inf.ethz.ch/summersa/wiki/doku.php?id=personal), and if you need to contact me (feel free), my email address is alexander dot summers at inf dot ethz dot ch

## Selected Publications

{% bibliography --file selected.bib %}
